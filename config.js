exports.LISTEN_PORT = 3002;
exports.HBBTV_MARKS_TOPIC = "dataitem.add";
exports.ZOOKEEPER_HOSTS = "worker4.konodrac.local:2181,worker2.konodrac.local:2181,worker3.konodrac.local:2181";
exports.TRUST_PROXY = true;

exports.SLACK_WEBHOOK_URI = "https://hooks.slack.com/services/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";
exports.SLACK_USERNAME = "aflow-marks-collector";

exports.MONGO_URI = "mongodb://mongodb.konodrac.local:27017"
exports.MONGO_DB_NAME = "konocloud"

exports.QUERY_PARAMS_LIMIT = 100;
exports.QUERY_PARAM_CHARACTER_LIMIT = 255;
